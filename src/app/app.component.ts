import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  name: string;
  description: string;
  style: string;
  production: string;
  date: Date;
  from: string;
  titles: Array<string>;
  usernames: Array<string>;
  username: string;

  constructor() {
    this.titles = [
      'Fight for your right',
      'Neil Young',
      'Robert Smith',
      'Joey Ramones',
      'Daniel Johnston',
    ];
    this.usernames = [
      'Neil Young',
      'Robert Smith',
      'Joey Ramones',
      'Daniel Johnston',
    ];
    this.name = 'The Cure';
    this.description = 'CureCureCureCure';
    this.style = 'Folk - New Wave';
    this.production = 'BBC live 1972';
    this.style = '';
    this.from = '';
    this.username = 'Town W Zant';
  }

  bandImgList = [
    '../assets/images/SHELLAC-1000-Hurts.jpg',
    '../assets/images/toxicheart.jpeg',
    '../assets/images/valium.jpeg',
  ];

  bandName = 'The Cure';
  bandDescription = 'CureCureCureCure';
  bandStyle = 'Folk - New Wave';
  bandProduction = 'BBC live 1972';
  bandstyle = '';
  bandFrom = '';
  bandNameList: [
    {
      name: 'The Cure';
    },
    {
      name: 'Eath';
    },
    {
      name: 'The Ramones';
    }
  ];

  bandPictureUrlList = [
    'https://robohash.org/xp?set=set4',
    'https://robohash.org/xp1?set=set4',
    'https://robohash.org/xp2?set=set4',
  ];
  isAvailable = false;
  selectedPictureIndex = 0;

  getSelectedPictureUrl() {
    return this.bandPictureUrlList[this.selectedPictureIndex];
  }

  selectNextPicture() {
    this.selectedPictureIndex =
      (this.selectedPictureIndex + 1) % this.bandPictureUrlList.length;
  }

  selectPictureWithIndex(index) {
    this.selectedPictureIndex = index;
  }

  changeUsername() {
    this.username = 'Joey Ramones';
  }
}
