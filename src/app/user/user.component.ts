import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  username: string;
  usernames: Array<string>;
  disabled: boolean;
  @Input() titles: string;
  @Output() changeUsernameEmitter = new EventEmitter();

  constructor() {
    this.username = 'Town Wan Zant';
    this.usernames = [
      'Neil Young',
      'Robert Smith',
      'Joey Ramones',
      'Daniel Johnston',
    ];
    this.disabled = true;
  }

  ngOnInit(): void {}

  onClickButton() {
    this.disabled = !this.disabled;
  }

  onClickChangeUsername() {
    this.changeUsernameEmitter.emit();
  }
}
