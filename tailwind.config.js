module.exports = {
  mode: "jit",
  purge: {
    enabled: true,
    content: ["./src/**/*.{html,ts}"],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      blur: ["hover", "focus"],
      blur: {
        xs: "2px",
      },
      backdropBlur: {
        xs: "2px",
      },
    },

    container: {
      center: true,
    },
  },
  variants: {
    extend: {
      opacity: ["disabled"],
    },
  },
  plugins: [],
};
