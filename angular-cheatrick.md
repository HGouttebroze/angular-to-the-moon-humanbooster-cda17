# Angular

# La gestion du DOM

### Les directives

On retrouve 2 sortes:

- structurelles
  - ngIf: j affiche ou nonkke chose
  - ngFor permet de boucler
- par attributs

### Directive structurelle ngIf

### Directive structurelle ngFor

exemple:

- boucler sur la variabe `usernames`
- récupérer l'index, i, du `username`

  `*ngFor="let username of usernames; let i = index"`

to try:
\*ngIf="let title of titles; let i = index; let isEven = even; let isLast = last;let isFirst = first; let isOdd = odd;">
<span>[title.i] === isOdd ? [class]="text-pink-600" : [class]="text-purple-600"</span>

```ts
  changeUsername(index: number) {
    this.username[index] = 'Joey Ramones';
  }
```
